import os
import json
from typing import Optional, List

from pydantic import BaseModel, create_model
from datetime import datetime
from discord import Guild, TextChannel, ForumChannel

botDirectory = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../')

class WizxConfig(BaseModel):
	auth: create_model('Auth',
		discord_token=(str, ...),
		geocoding_token=(str, ...)
	)

	bastard_mode: bool = False
	silent_mode: bool = False

	home_guild_id: Optional[int] 			= None
	home_guild: Optional[Guild] 			= None

	welcome_channel_id: Optional[int] 		= None
	mod_channel_id: Optional[int] 			= None
	tickets_channel_id: Optional[int]		= None
	query_channel_id: Optional[int] 		= None
	test_channel_id: Optional[int] 			= None
	voice_channel_id: Optional[int] 		= None

	class Channels(BaseModel):
		welcome: Optional[TextChannel] 		= None
		mod: Optional[TextChannel] 			= None
		tickets: Optional[ForumChannel]		= None
		query: Optional[ForumChannel] 		= None
		test: Optional[TextChannel] 		= None
		voice: Optional[TextChannel] 		= None

		class Config:
			arbitrary_types_allowed = True
	
	channels: Channels = Channels()

	query_mod_ids: Optional[List[int]] 		= None

	# test_message: Optional[Message] = None

	class Temporium(BaseModel):
		role_delimiter: str = 'tz|'
		output_format: str = '%H:%M'

	temporium: Temporium = Temporium()

	class Query(BaseModel):
		output_file: str = 'queries.json'
		post_time: str = 'Saturday 14:00'

	query: Query = Query()

	class Standup(BaseModel):
		post_time: str = 'Sunday 15:00'

	standup: Standup = Standup()

	class Tickets(BaseModel):
		check_time: str = 'Sunday 22:00'
		designation_amount: int = 6
		undesignation_amount: int = 4

	tickets: Tickets = Tickets()

	class Config:
		arbitrary_types_allowed = True

try:
	with open(os.path.join(botDirectory, "auth.json"), 'r') as f:
		auth = json.load(f)
except FileNotFoundError:
	raise PermissionError('You do not have an authentication file. Please load auth.json from the AUTH project variable.')

with open(os.path.join(botDirectory, "wizx.config.json"), 'r') as f:
	config = WizxConfig(**{'auth': auth, **json.load(f)})
