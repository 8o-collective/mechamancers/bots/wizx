import json
from datetime import timedelta, timezone

from dateutil.parser import parse

from wizx import config
from wizx.utils.string import style
from wizx.utils.scheduler import schedule

@schedule(
	post_time=parse(config.query.post_time, fuzzy=True).replace(tzinfo=timezone.utc), 
	repeat_every=timedelta(days=7)
)
async def post_query():
	try:
		with open('queries.json', 'r') as f:
			queries = json.load(f)
			assert len(queries) > 0
	except (FileNotFoundError, json.decoder.JSONDecodeError, AssertionError):
		return await config.channels.welcome.send(style('alas, no queries now remain,,,'))
		
	question = queries[0]
	queries = queries[1:]

	with open('queries.json', 'w+') as f:
		json.dump(queries, f, indent=4)

	await config.channels.query.create_thread(name=question, content=f'@everyone {question}')