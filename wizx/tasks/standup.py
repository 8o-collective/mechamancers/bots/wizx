from datetime import datetime, timedelta, timezone

from dateutil.parser import parse

from pydantic import create_model

from wizx import config
from wizx import client

from wizx.utils.string import style
from wizx.utils.scheduler import schedule
from wizx.utils.generator import find

# create a state model with default time of config.standup.post_time
state = create_model('State', standup_time=parse(config.standup.post_time, fuzzy=True).replace(tzinfo=timezone.utc))()

def event_name(date):
	return f"Standup - {date.strftime('%d/%m/%Y')}"

def get_standup_event():
	all_events = config.home_guild.scheduled_events
	return find(event for event in all_events if event.name == event_name(state.standup_time))


async def create_standup_event():
	await config.home_guild.create_scheduled_event(
		name=event_name(state.standup_time),
		start_time=state.standup_time,
		end_time=state.standup_time + timedelta(hours=1),
		channel=config.channels.voice,
	)

async def standup_maintenance():
	print(len(config.channels.tickets.available_tags))
	if len(config.channels.tickets.available_tags) > 18:
		await config.channels.welcome.send(f'@founders {style("The oath brands have filled mine skin and the flesh is tearing! Prithee, remove some tags!")}')

	await config.channels.tickets.create_tag(name=f"week: {state.standup_time.strftime('%d/%m/%Y')}", emoji="🗓️") # create new weekly tag for oaths

	state.standup_time += timedelta(days=7)
	await create_standup_event()

@schedule(
	post_time=state.standup_time - timedelta(hours=24), 
	repeat_every=timedelta(days=7)
)
async def post_standup_reminder():
	if not get_standup_event():
		await create_standup_event() # create event if it hasn't been created yet

	await config.channels.welcome.send(f'@everyone {style("add thineself to the Event if ")}\'{style("tis true thee can attend")}\n\n{get_standup_event().url}')

@schedule(
	post_time=state.standup_time, 
	repeat_every=timedelta(days=7)
)
async def post_standup():
	last_standup_event = get_standup_event()
	try:
		await last_standup_event.start()
	except:
		pass
	await config.channels.welcome.send(style('Standup is commencing posthaste. The following users have made claim of their attendance:\n\n')
				    +'\n'.join([user.mention async for user in last_standup_event.users()])
					+f'\n\n{get_standup_event().url}')

	await standup_maintenance()