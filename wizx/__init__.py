from discord import Game

from mechascrypt import parse, exceptions, PARSABLE_INSTRUCTIONS
from mechascrypt.library import *
from mechascrypt.wrap import wrap

from wizx.config import config
from wizx.client import client
from wizx.docs import __version__, __doc__

from wizx.utils.string import style

from wizx.commands.temporium import temporium
from wizx.commands.query import query

from wizx.tasks.query import post_query
from wizx.tasks.standup import post_standup, post_standup_reminder

@client.event
async def on_ready():
    await client.change_presence(activity=Game('hogworm'))

    config.home_guild = client.get_guild(config.home_guild_id)

    for name, _ in config.channels:
        setattr(config.channels, name, client.get_channel(getattr(config, f"{name}_channel_id")))
    
    if config.silent_mode: 
        from discord import TextChannel
        async def send(self, *args, **kwargs): return None
        TextChannel.send = send

    scheduled_tasks = [post_query, post_standup, post_standup_reminder]

    for task in scheduled_tasks: task.start()

    if config.bastard_mode:
        await config.channels.welcome.send(style('I AM ON MY WAY. BEHOLD'))

    print(config.home_guild)

    print(f'{client.user.name} online.')

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    
    ## WIZX STANDARD LIBRARY
    commands = [temporium, query]

    for command in wrap(commands, message):
        globals()[command.__name__] = command
    
    if any(message.content.startswith(instruction) for instruction in PARSABLE_INSTRUCTIONS):
        # if config.bastard_mode:
        #     if message.content.split(' ')[-1] != 'PLEASE':
        #         await message.channel.send(style('and WHERE Are your manners young man ?')) 
        #         return 

        try:
            program = parse(message.content, execute=False)

            result = await eval(compile(program, filename="<ast>", mode="eval"))
        except exceptions.UnexpectedToken as e:
            print(e)
            # result = style('WHAT prithee cast thou!!')
            return
        except exceptions.CommandNotFoundError as e:
            print(e)
            return
        except exceptions.DocsNotFoundError as e:
            print(e)
            result = style('thoust request\'d symbol has Not an informatic !')
        except NameError as e:
            print(e)
            # result = style('i have NOT been made familiar with such a spelle !')
            return
        except PermissionError as e:
            print(e)
            result = style('thou needes to be a Grand Wizard to caste these spelles!!')
        except Exception as e:
            print(e)
            result = style(f'as UNFAMILIAR as i am with such Chicanery i suspect it is so:\n\n {e}')
        
        await message.channel.send(result)
    
    if message.content.startswith('#!MECHASCRYPT\n'):
        try:
            program = parse(message.content)

            exec(compile(program, filename="<ast>", mode="exec"))
            result = style(f'the scrypt has been valued and is perfectly acceptable :)')
        except Exception as e:
            result = style(f'thine Bulleshit has been utterly obliterated:\n\n {e}')
        
        await message.channel.send(result)