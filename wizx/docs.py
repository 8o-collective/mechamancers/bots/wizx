import subprocess

from wizx.utils.string import style

__version__ = '0.1.0'
__branch__ = 'main'
__hash__ = subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode('ascii').strip() # depends on git

__doc__ = style(f"you are gazing upone WIZX v{__version__} (`{__hash__[:8]}`), the mostest sofisticated Wizard in all the landes.")