import urllib.request
import json
from datetime import datetime

from zoneinfo import ZoneInfo
from dateutil.parser import parse

from wizx import config

from wizx.utils.string import style, sanitize
from wizx.utils.generator import find

get_zone_from_name = lambda name: ZoneInfo(name[len(config.temporium.role_delimiter):])

def get_zone_from_user(target):
	zonerole = find(role for role in target.roles if config.temporium.role_delimiter in role.name)
  
	if zonerole == None:
		raise KeyError

	return get_zone_from_name(zonerole.name)

async def temporium(set=None, at=None, on=None, compute=False, message=None):
	'''
	Calculate temporal relativism according to local spacetime curvature of 8oC members.

	Parameters
	----------
	set : str
		location to set yourself to

	at : str
		time to calculate for
	on : @user
		user to check the time of
	compute : bool
		if true, temporium will return UNIX time
	'''

	if isinstance(set, str):
		if set == '':
			return style('how the FUCK am i supposed to know what timezone you are in')

		query = set.replace(' ', '+').lower()

		async with message.channel.typing():
			try:
				with urllib.request.urlopen(f'http://api.positionstack.com/v1/forward?access_key={config.auth.geocoding_token}&timezone_module=1&query={query}') as response:
					try:
						data = json.loads(response.read())['data'][0]
					except IndexError:
						return style('i have literally *never* heard of that place')
			except:
				return style('input malformed (too small or has non-ascii)')

		timezone = data['timezone_module']

		role_name = config.temporium.role_delimiter + timezone['name']

		for role in message.author.roles:
			if config.temporium.role_delimiter in role.name:
				if len(role.members) == 1:
					await role.delete()
				else:
					await message.author.remove_roles(role)

		for role in config.home_guild.roles:
			if role.name == role_name:
				await message.author.add_roles(role)
				return style('set ' + timezone['name'])
		
		zone_role = await config.home_guild.create_role(name=role_name)

		await message.author.add_roles(zone_role)

		return style('created and set ' + timezone['name'])


	targets = [member for member in config.home_guild.members if find(role for role in member.roles if config.temporium.role_delimiter in role.name)]


	if isinstance(on, str):
		on = on.strip()

		if on == '':
			return style('you FOOL!! thou hast hubrise\'d to specify a subjkect')
		
		target = find(member for member in config.home_guild.members if str(member.id) == on[2:-1]) 

		if target == None:
			return style('thoust target is Invalid!')
		
		# try:
		# 	timezone = get_zone_from_user(target)
		# except KeyError:
		# 	return style('could not Divine target\'s timezone')
		
		targets = [target]

	
	try:
		timezone = get_zone_from_user(message.author)
	except KeyError:
		return style('could not Divine your timezone')

	time = datetime.now(tz=timezone)

	if isinstance(at, str):
		if at == '':
			return style('no time????????? what')
		
		time = parse(at, fuzzy=True).replace(tzinfo=timezone)


	if compute == True:
		parsedtime = time.astimezone(tz=ZoneInfo('UTC'))

		unixtime = int(parsedtime.timestamp())

		return f'<t:{unixtime}:F>, <t:{unixtime}:R>, \<t:{unixtime}>'


	converttime = lambda tz: time.astimezone(tz)

	times = [(member.name, converttime(get_zone_from_user(member)).strftime('%H:%M')) for member in targets]

	output = {}
	for name, time in times:
		output.setdefault(time, []).append(name)
	
	sortedoutput = dict(sorted(output.items()))

	return style(
		'\n'.join([f'{sanitize(", ".join(name))}: {time}' for time, name in sortedoutput.items()]) 
	)