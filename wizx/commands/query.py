import json

from wizx import config
from wizx.utils.string import style

async def query(question=None, check=False, delete=None, like=None, dislike=None, love=None, despise=None, message=None):
	'''
	Ask questions of the Gods, which will be answered every week.

	Parameters
	----------
	question : str
		a query to be asked

	check : bool, moderator only
		check the current query list
	delete : str, moderator only
		id of query to delete
	like : str, moderator only
		id of query to move up in queue
	dislike : str, moderator only
		id of query to move down in queue
	love : str, moderator only
		id of query to move to top of queue
	despise: str, moderator only
		id of query to move to bottom of queue
	'''

	try: 
		with open('queries.json', 'r') as f:
			queries = json.load(f)
	except (FileNotFoundError, json.decoder.JSONDecodeError):
		queries = []
	
	def write_queries(queries):
		with open('queries.json', 'w') as f:
			json.dump(queries, f, indent=4)

	def format_and_write_queries(queries):
		write_queries(queries)
		return '\n'.join([f'{i}: {query}' for i, query in enumerate(queries)]) if len(queries) != 0 else style('alas, no queries now remain,,,')

	if (check or delete or like or dislike or love or despise) and message.author.id not in config.query_mod_ids:
		raise PermissionError
	
	if check:
		return format_and_write_queries(queries)
	
	if delete:
		queries.pop(int(delete))
		write_queries(queries)
		return style('query hast been Eugenicized into the aethers!')
	
	if like and like != 0:
		queries.insert(int(like) - 1, queries.pop(int(like)))
		return format_and_write_queries(queries)
	
	if dislike and int(dislike) != len(queries):
		queries.insert(int(dislike) + 1, queries.pop(int(dislike)))
		return format_and_write_queries(queries)
	
	if love:
		queries.insert(0, queries.pop(int(love)))
		return format_and_write_queries(queries)
	
	if despise:
		queries.insert(len(queries), queries.pop(int(despise)))
		return format_and_write_queries(queries)

	if not isinstance(question, str) or question == '':
		return style('no queery found (other than you)')
	
	queries += [question]

	write_queries(queries)
	
	return style('thy suggestion shall be considered (within many a business fortnight)')