from datetime import datetime, timedelta, timezone

from discord.ext import tasks

class schedule():
	def __init__(self, post_time, repeat_every):
		self.next_post_time = post_time
		self.time_change = repeat_every
	
	def increment_time(self):
		self.next_post_time += self.time_change

	def __call__(self, func):
		@tasks.loop(seconds=5) 
		async def inner(*args, **kwargs):
			if datetime.now(tz=timezone.utc) > self.next_post_time:
				self.increment_time()
				await func(*args, **kwargs)

		return inner