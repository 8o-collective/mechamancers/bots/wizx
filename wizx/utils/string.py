normal_alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

styled_alphabet = '𝔞𝔟𝔠𝔡𝔢𝔣𝔤𝔥𝔦𝔧𝔨𝔩𝔪𝔫𝔬𝔭𝔮𝔯𝔰𝔱𝔲𝔳𝔴𝔵𝔶𝔷𝔄𝔅ℭ𝔇𝔈𝔉𝔊ℌℑ𝔍𝔎𝔏𝔐𝔑𝔒𝔓𝔔ℜ𝔖𝔗𝔘𝔙𝔚𝔛𝔜ℨ'

font_dictionary = { normal: styled_alphabet[i] for i, normal in enumerate(normal_alphabet) }

def style(s):
	styled = ''
	should_style = True

	for letter in s:
		if letter == '`':
			should_style = not should_style
		
		styled += font_dictionary[letter] if letter in normal_alphabet and should_style else letter

	return styled

def sanitize(s):
	return s.replace('_', '\_').replace('*', '\*').replace('~', '\~')